import { Test, TestingModule } from '@nestjs/testing';
import { EbayGlobalService } from './ebay-global.service';

describe('EbayGlobalService', () => {
  let service: EbayGlobalService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EbayGlobalService],
    }).compile();

    service = module.get<EbayGlobalService>(EbayGlobalService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
