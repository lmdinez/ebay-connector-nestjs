import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { map, Observable } from 'rxjs';
@Injectable()
export class EbayGlobalService {
  constructor(private httpService: HttpService) {}

  ebayHttpRequest(options, header, method, url): Observable<any> {
    try {
      return this.httpService
        .request({
          baseURL: url,
          method: method,
          data: options,
          headers: header,
        })
        .pipe(map((response) => response));
    } catch (error) {
     
      
    }
  }
}
