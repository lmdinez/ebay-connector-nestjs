import { HttpModule } from '@nestjs/axios';
import { Global, Module } from '@nestjs/common';
import { EbayGlobalService } from './ebay-globel/ebay-global.service';
@Global()
@Module({
  imports: [HttpModule.register({})],
  providers: [EbayGlobalService],
  exports: [EbayGlobalService],
})
export class EbayGlobelModule {}
