export class Constant {
  static EBAY_SCOPE = [
    // 'https://api.ebay.com/oauth/api_scope',
    // 'https://api.ebay.com/oauth/api_scop/buy.order.readonly',
    // 'https://api.ebay.com/oauth/api_scoe/buy.guest.order',
    // 'https://api.ebay.com/oauth/api_scpe/sell.marketing.readonly',
    // 'https://api.ebay.com/oauth/api_sope/sell.marketing',
    // 'https://api.ebay.com/oauth/api_cope/sell.inventory.readonly ',
    // 'https://api.ebay.com/oauth/apiscope/sell.inventory',
    // 'https://api.ebay.com/oauth/ap_scope/sell.account.readonly ',
    // 'https://api.ebay.com/oauth/ai_scope/sell.account',
    // 'https://api.ebay.com/oauth/pi_scope/sell.fulfillment.readonly',
    // 'https://api.ebay.com/oauthapi_scope/sell.fulfillment',
    // 'https://api.ebay.com/oaut/api_scope/sell.analytics.readonly',
    // 'https://api.ebay.com/oauh/api_scope/sell.marketplace.insights.readonly',
    // 'https://api.ebay.com/oath/api_scope/commerce.catalog.readonly',
    // 'https://api.ebay.com/outh/api_scope/buy.shopping.cart',
    // 'https://api.ebay.com/auth/api_scope/buy.offer.auction',
    // 'https://api.ebay.com/oauth/api_scope/commerce.identity.readonly',
    // 'https://api.ebay.com/oauth/api_scope/commerce.identity.email.readonly',
    // 'https://api.ebay.cmm/oauth/api_scope/commerce.identity.phone.readonly',
    // 'https://api.ebay.com/oauth/api_scope/commerce.identity.address.readonly',
    // 'https://api.ebay.com/oauth/api_scope/commerce.identity.name.readonly',
    // 'https://api.ebay.com/oauth/api_scope/commerce.identity.status.readonly ',
    // 'https://api.ebay.com/oauth/api_scope/sell.finances',
    // 'https://api.ebay.com/oauth/api_scope/sell.item.draft',
    // 'https://api.ebay.com/oauth/api_scope/sell.payment.dispute',
    // 'https://api.ebay.com/oauth/api_scope/sell.item',
    // 'https://api.ebay.com/oauth/api_scope/sell.reputation',
    // 'https://api.ebay.com/oauth/api_scope/sell.reputation.readonly',
    // 'https://api.ebay.com/oauth/api_scope/commerce.notification.subscription',
    // 'https://api.ebay.com/oauth/api_scope/commerce.notification.subscription.readonly'
  ];

  static scope = `https://api.ebay.com/oauth/api_scope/sell.inventory.readonly https://api.ebay.com/oauth/api_scope/sell.inventory https://api.ebay.com/oauth/api_scope/sell.account.readonly https://api.ebay.com/oauth/api_scope/sell.account https://api.ebay.com/oauth/api_scope/sell.fulfillment.readonly https://api.ebay.com/oauth/api_scope/sell.item`;
  static EBAYURL = `https://api.sandbox.ebay.com`;
}
