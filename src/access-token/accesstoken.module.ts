/*
https://docs.nestjs.com/modules
*/

import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { LoggerModule } from 'src/logger/logger.module';
import { TokenModule } from 'src/token/token.module';
import { AccessTokenController } from './accesstoken.controller';
import { AccessTokenService } from './accesstoken.service';

@Module({
  imports: [ScheduleModule.forRoot(), TokenModule, LoggerModule],
  controllers: [AccessTokenController],
  providers: [AccessTokenService],
})
export class AccessTokenModule {}
