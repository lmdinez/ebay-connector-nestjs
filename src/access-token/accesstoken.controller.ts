/*
https://docs.nestjs.com/controllers#controllers
*/

import { Controller } from '@nestjs/common';
import { Cron, CronExpression, SchedulerRegistry } from '@nestjs/schedule';
import { LoggerService } from 'src/logger/logger.service';
import { TokenService } from 'src/token/token.service';
var moment = require('moment');

@Controller()
export class AccessTokenController {
  constructor(
    private schedulerRegistry: SchedulerRegistry,
    private tokenService: TokenService,
    private loggerService: LoggerService,
  ) {}
  @Cron(CronExpression.EVERY_30_MINUTES, {
    name: 'messaging',
    timeZone: 'Asia/Kolkata',
  })
  async triggerMessage() {
    const data = await this.tokenService.getAllTokens();

    await data.forEach((element) => {
      setTimeout(() => {
        //curennt date and time
        const now = moment().format('YYYY-MM-DD HH:mm:ss');
        // current date time - updatedAt date time
        const before = Date.parse(
          moment(now).subtract(3600, 'seconds').format('YYYY-MM-DD HH:mm:ss'),
        );
        // updated date and time
        const updated = Date.parse(
          moment(element.updatedAt).format('YYYY-MM-DD HH:mm:ss'),
        );
        if (before >= updated) {
          this.loggerService.log(
            'Access token Successfully Updated',
            element.client_id,
          );
          return this.tokenService.send(element);
        } else {
          // console.log("j")
          this.loggerService.log('Tokken already Updated', element.client_id);
        }
      }, 5000);
    });
  }
}
