import { LoggerModule } from './logger/logger.module';
import { AccessTokenModule } from './access-token/accesstoken.module';
import { RedisCacheModule } from './redis/redis-cache.module';
import { SellerModule } from './seller/seller.module';
import { ProductModule } from './product/product.module';
import { Module } from '@nestjs/common';
import configService from './config/config.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import { TokenModule } from './token/token.module';
import { EbayGlobelModule } from './ebay-globel/ebay-globel.module';
import { KafkaModule } from './kafka/kafka.module';

@Module({
  imports: [
    LoggerModule,
    AccessTokenModule,
    RedisCacheModule,
    SellerModule,
    ProductModule,
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [configService],
      isGlobal: true,
    }),
    /** Configure TypeOrm asynchronously. */
    TypeOrmModule.forRootAsync({
      useFactory: async (
        configService: ConfigService,
      ): Promise<PostgresConnectionOptions> =>
        configService.get('postgresDatabase'),
      inject: [ConfigService],
    }),

    // AuthModule,
    TokenModule,
    EbayGlobelModule,
    KafkaModule,
  ],
})
export class AppModule {}
