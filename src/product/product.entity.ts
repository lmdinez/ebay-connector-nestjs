import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('product')
export class ProductEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column('jsonb', { nullable: true })
  productData?: any;
}
