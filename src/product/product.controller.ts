/*
https://docs.nestjs.com/controllers#controllers
*/

import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse } from '@nestjs/swagger';
import { ProductDto } from './product-dto';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) {}

  @Post()
  @ApiCreatedResponse({ description: 'Product create Body' })
  @ApiBody({ type: ProductDto })
  async createProduct(@Body() data: ProductDto) {
    const obj = {
      productData: data,
    };

    return {
      statusCode: HttpStatus.OK,
      message: 'Product added successfully',
      data: await this.productService.createProduct(obj),
    };
  }

  // @Get("notify")
  // async notify(@Query() data:NotifyDto){
  //     console.log("notify",data);

  // }
}
