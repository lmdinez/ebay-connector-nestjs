/*
https://docs.nestjs.com/providers#services
*/

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductEntity } from './product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(ProductEntity)
    private productRepository: Repository<ProductEntity>,
  ) {}

  async createProduct(data: any) {
    const productData = this.productRepository.create(data);
    await this.productRepository.save(data);
    return productData;
  }

  async getByProductCollection(id: number) {
    return await this.productRepository.findOne({ where: { id: id } });

    // return this.productRepository
    //     .query(`SELECT * from product WHERE ${id} = ANY(productData)`);
  }
}
