import { ApiProperty } from '@nestjs/swagger';

export class ProductFamilyProperties {
  @ApiProperty({
    type: String,
    description: 'notes',
  })
  make: string;

  @ApiProperty({
    type: String,
    description: 'notes',
  })
  model: string;

  @ApiProperty({
    type: String,
    description: 'notes',
  })
  year: string;

  @ApiProperty({
    type: String,
    description: 'notes',
  })
  trim: string;

  @ApiProperty({
    type: String,
    description: 'notes',
  })
  engine: string;
}

export class CompatibleProducts {
  @ApiProperty({
    type: ProductFamilyProperties,

    // additionalProperties: {
    //     ProductFamilyProperties
    // }
  })
  productFamilyProperties: ProductFamilyProperties;

  @ApiProperty({
    type: String,
    description: 'notes',
  })
  notes: string;
}

export class ProductDto {
  @ApiProperty({
    isArray: true,
    type: CompatibleProducts,
  })
  compatibleProducts: CompatibleProducts[];
}

export class NotifyDto {
  @ApiProperty({
    type: String,
    description: 'collectionId',
  })
  sellerId: string;

  @ApiProperty({
    type: String,
    description: 'collectionId',
  })
  platformId: string;

  @ApiProperty({
    type: String,
    description: 'collectionId',
  })
  collectionId: string;
}
