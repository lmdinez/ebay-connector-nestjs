import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RefreshTokenEntity } from './RefreshToken.entity';
import { TokenController } from './token.controller';
import { TokenService } from './token.service';
import { KafkaModule } from 'src/kafka/kafka.module';
import { EbayGlobelModule } from 'src/ebay-globel/ebay-globel.module';
import { LoggerModule } from 'src/logger/logger.module';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([RefreshTokenEntity]),
    KafkaModule,
    EbayGlobelModule,
    LoggerModule,
  ],
  providers: [TokenService],
  controllers: [TokenController],
  exports: [TokenService],
})
export class TokenModule {}
