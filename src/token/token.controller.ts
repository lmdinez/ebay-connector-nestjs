import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { Ctx, KafkaContext, MessagePattern } from '@nestjs/microservices';
import { ApiBody, ApiCreatedResponse } from '@nestjs/swagger';
import { EbayGlobalService } from 'src/ebay-globel/ebay-globel/ebay-global.service';
import { KafkaService } from 'src/kafka/kafka/kafka.service';
import { TokenDto } from './token.dto';
import { TokenService } from './token.service';

@Controller('token')
export class TokenController {
  constructor(
    private tokenService: TokenService,
    private kafkaService: KafkaService,
    private ebayGlobalService: EbayGlobalService,
  ) {}

  @Post()
  @ApiCreatedResponse({ description: 'Create token' })
  @ApiBody({ type: TokenDto })
  async createClientTokenDetails(@Body() data: TokenDto) {
    // return this.tokenService.sendToKafka(data)
    const topicName = 'refresh_token';
    const messageType = 'Create user details';
    return this.kafkaService.send(data, topicName, messageType);
  }

  @MessagePattern('refresh_token')
  async sendTokenUserDetails(@Ctx() context: KafkaContext) {
    const originalMessage = context.getMessage();
    console.log('originalm', originalMessage);
    const data = await this.tokenService.createUserTokenDataKafkaToDb(
      originalMessage.value['body'],
    );
    console.log('gg', data);
    this.tokenService.send(data);
    return {
      statusCode: HttpStatus.OK,
      message: 'Movie added successfully',
      data: data,
    };
  }

  @MessagePattern('access_token')
  async sendToken(@Ctx() context: KafkaContext) {
    const originalMessage = context.getMessage();
    return {
      statusCode: HttpStatus.OK,
      message: 'Access token generated successfully',
      data: await this.tokenService.updateAccessToken(
        originalMessage.value['body']['client_id'],
        originalMessage.value['body'],
      ),
    };
  }
}
