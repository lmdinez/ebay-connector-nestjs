import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { lastValueFrom } from 'rxjs';
import { Constant } from 'src/constant';
import { EbayGlobalService } from 'src/ebay-globel/ebay-globel/ebay-global.service';
import { KafkaService } from 'src/kafka/kafka/kafka.service';
import { Repository } from 'typeorm';
import { RefreshTokenEntity } from './RefreshToken.entity';
import { TokenDto } from './token.dto';
var base64 = require('base-64');
var queryString = require('query-string');
@Injectable()
export class TokenService {
  constructor(
    @InjectRepository(RefreshTokenEntity)
    private tokenRepository: Repository<RefreshTokenEntity>,
    private kafkaService: KafkaService,
    private ebayGlobalService: EbayGlobalService,
  ) {}

  async getAllTokens() {
    return await this.tokenRepository.find();
  }

  async createUserTokenDataKafkaToDb(value) {
    try {
      const data = this.tokenRepository.create(value);
      await this.tokenRepository.save(value);
      return data;
    } catch (error) {}
  }

  async getByClient(id: number) {
    try {
      return await this.tokenRepository.findOne({ where: { id: id } });
    } catch (error) {}
  }

  async updateAccessToken(
    client_id: string,
    accessTokenData: Partial<TokenDto>,
  ) {
    await this.tokenRepository.update({ client_id }, accessTokenData);
    return await this.tokenRepository.findOne({ client_id });
  }

  async send(data) {
    const scope = Constant.scope;
    const details = {
      grant_type: 'refresh_token',
      refresh_token: data.refresh_token,
      scope: scope,
    };
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization:
        'Basic ' + base64.encode(`${data.client_id}:${data.client_secret}`),
    };
    const method = 'POST';
    const url = `${Constant.EBAYURL}/identity/v1/oauth2/token`;
    const tokValue$ = this.ebayGlobalService.ebayHttpRequest(
      queryString.stringify(details),
      headers,
      method,
      url,
    );
    const tokenData = await lastValueFrom(tokValue$);
    const payload = tokenData.data;
    payload.client_id = data.client_id;
    const topicName = 'access_token';
    const messageType = 'generate access token';
    return this.kafkaService.send(payload, topicName, messageType);
  }
}
