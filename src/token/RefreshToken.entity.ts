import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('refresh_token')
export class RefreshTokenEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    nullable: false,
    length: 200000,
  })
  refresh_token: string;

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
    length: 500,
  })
  client_id: string;
  @Column({
    type: 'varchar',
    length: 500,
  })
  client_secret: string;
  @Column({
    type: 'varchar',
    length: 200000,
  })
  access_token: string;

  @Column()
  token_type: string;

  @Column()
  expires_in: number;

  @CreateDateColumn()
  public createdAt;

  @UpdateDateColumn()
  public updatedAt;
}
