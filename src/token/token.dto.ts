import { ApiProperty } from '@nestjs/swagger';

export class TokenDto {
  @ApiProperty({
    type: String,
    description: 'Referesh Token',
  })
  refresh_token: string;

  @ApiProperty({
    type: String,
    description: 'Client ID',
  })
  client_id: string;

  @ApiProperty({
    type: String,
    description: 'Client Secret',
  })
  client_secret: string;

  @ApiProperty({
    type: String,
    description: 'token_type',
  })
  token_type: string;

  @ApiProperty({
    type: String,
    description: 'access_token',
  })
  access_token: string;
  @ApiProperty({
    type: Number,
    description: 'expires_in',
  })
  expires_in: number;
}
