/*
https://docs.nestjs.com/providers#services
*/

import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class LoggerService {
  logger: Logger;
  constructor() {
    this.logger = new Logger();
  }

  /**
   * Write a 'log' level log.
   */
  async log(message: any, optionalParams: any) {
    await this.logger.log('Message:' + message + ' Status :' + optionalParams);
  }

  /**
   * Write an 'error' level log.
   */
  async error(message: any, optionalParams: any) {
    await this.logger.error(
      'Message:' + message + ' Status :' + optionalParams,
    );
  }

  /**
   * Write a 'warn' level log.
   */
  async warn(message: any, optionalParams: any) {
    await this.logger.error('Message:' + message + 'Status :' + optionalParams);
  }

  /**
   * Write a 'debug' level log.
   */
  async debug?(message: any, optionalParams: any) {
    await this.logger.debug('Message:' + message + 'Status :' + optionalParams);
  }

  /**
   * Write a 'verbose' level log.
   */
  async verbose?(message: any, optionalParams: any) {
    await this.logger.verbose(
      'Message:' + message + 'Status :' + optionalParams,
    );
  }
}
