import { Injectable } from '@nestjs/common';
import { Client, ClientKafka } from '@nestjs/microservices';
import { microserviceConfig } from 'src/microserviceConfig';
import { KafkaPayload } from '../kafka.message';

@Injectable()
export class KafkaService {
  @Client(microserviceConfig)
  client: ClientKafka;

  async send(payload, topicName, messageType) {
    const data: KafkaPayload = {
      messageId: '' + new Date().valueOf(),
      body: payload,
      messageType: messageType,
      topicName: topicName, // topic name could be any name
    };
    const value = await this.client.emit(topicName, JSON.stringify(data));

    return value;
  }
}
