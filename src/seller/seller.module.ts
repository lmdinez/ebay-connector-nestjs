import { SellerController } from './seller.controller';
import { SellerService } from './seller.service';
/*
https://docs.nestjs.com/modules
*/

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SellerEntity } from './seller.entity';
import { ProductModule } from 'src/product/product.module';
import { TokenModule } from 'src/token/token.module';
import { HttpModule } from '@nestjs/axios';
import { RedisCacheModule } from 'src/redis/redis-cache.module';
import { LoggerModule } from 'src/logger/logger.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([SellerEntity]),
    ProductModule,
    TokenModule,
    HttpModule,
    RedisCacheModule,
    LoggerModule,
  ],
  controllers: [SellerController],
  providers: [SellerService],
})
export class SellerModule {}
