import { Controller, Get, Query } from '@nestjs/common';
import { Ctx, KafkaContext, MessagePattern } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { Constant } from 'src/constant';
import { EbayGlobalService } from 'src/ebay-globel/ebay-globel/ebay-global.service';
import { KafkaService } from 'src/kafka/kafka/kafka.service';
import { LoggerService } from 'src/logger/logger.service';
import { ProductService } from 'src/product/product.service';
import { TokenService } from 'src/token/token.service';
import { listingDto, SellerDto } from './seller-dto';
import { SellerService } from './seller.service';

@Controller('seller')
export class SellerController {
  accessToken: any;
  retryPayload = {};
  retry: number = 0;
  constructor(
    private sellerService: SellerService,
    private kafkaService: KafkaService,
    private productService: ProductService,
    private tokenService: TokenService,
    private ebayGlobalService: EbayGlobalService,
    private loggerService: LoggerService,
  ) { }

  //Notify Sync Producer
  @Get()
  async createSeller(@Query() data: SellerDto) {
    const payload = {
      sellerId: data.sellerId,
      platformId: data.platformId,
      collectionId: data.collectionId,
    };

    const topicName = 'notify_sync';
    const messageType = 'notify_sync';
    return this.kafkaService.send(payload, topicName, messageType);
  }

  //Notify Sync Consumer
  @MessagePattern('notify_sync')
  async sendToken(@Ctx() context: KafkaContext) {
    const originalMessage = context.getMessage();
    const productColletionData =
      await this.productService.getByProductCollection(
        originalMessage.value['body']['collectionId'],
      );
    await this.sellerService.setSomeValue(
      originalMessage.value['body']['collectionId'],
      productColletionData['productData'],
    );
    //Create topic Platform classifier ex: ebay_product_collection

    const payload = {
      collectionId: originalMessage.value['body']['collectionId'],
      platformName:
        originalMessage.value['body']['platformId'] == 1
          ? 'ebay'
          : originalMessage.value['body']['platformId'] == 2
            ? 'etsy'
            : '',
      platformId: originalMessage.value['body']['platformId'],
      sellerId: originalMessage.value['body']['sellerId'],
      retrtLimt: 0,
    };
    const topicName =
      originalMessage.value['body']['platformId'] == 1
        ? 'ebay_product_collection'
        : originalMessage.value['body']['platformId'] == 2
          ? 'etsy_product_collection'
          : '';
    const messageType = 'product_data';
    return this.kafkaService.send(payload, topicName, messageType);
  }

  //ebay_product_collection Consumer
  @MessagePattern('ebay_product_collection')
  async ebay(@Ctx() context: KafkaContext) {
    const originalMessage = context.getMessage();
    var ebayValue = await originalMessage.value['body'];
    //redis
    try {
      const collectionPayload = await this.sellerService.getSomeValue(
        ebayValue['collectionId'],
      );
      this.accessToken = await this.tokenService.getByClient(ebayValue['sellerId']);
      const headers = {
        'Content-Language': 'en-US',
        Authorization: `Bearer ${this.accessToken['access_token']}`,
      };
      const method = 'PUT';
      const url = `${Constant.EBAYURL}/sell/inventory/v1/inventory_item/Watch/product_compatibility`;

      const tokValue$ = this.ebayGlobalService.ebayHttpRequest(
        collectionPayload,
        headers,
        method,
        url,
      );
      const tokenData = await lastValueFrom(tokValue$);
      const msg = 'Product Successfully Created !';
      this.loggerService.log(msg, tokenData.status);
    } catch (error) {
      console.log("erroe", error);

      const retrtLimt = 3;
      const count = ebayValue['retrtLimt'] + 1;
      if (retrtLimt < count) {
        return error;
      } else {
        const payload = {
          collectionId: ebayValue['collectionId'],
          platformName:
            ebayValue['platformId'] == 1
              ? 'ebay'
              : ebayValue['platformId'] == 2
                ? 'etsy'
                : '',
          platformId: ebayValue['platformId'],
          sellerId: ebayValue['sellerId'],
          topicName:
            ebayValue['platformId'] == 1
              ? 'ebay_product_collection'
              : ebayValue['platformId'] == 2
                ? 'etsy_product_collection'
                : '',
          retrtLimt: count,
        };
        const topicName = 'product_sync_error';
        const messageType = 'product_data';
        return this.kafkaService.send(payload, topicName, messageType);
      }
    }
  }

  //retry
  @MessagePattern('product_sync_error')
  async error(@Ctx() context: KafkaContext) {
    await setTimeout(() => {
      const originalMessage = context.getMessage();
      const value = originalMessage.value['body'];
      console.log(value);
      const messageType = 'product_data';
      return this.kafkaService.send(value, value['topicName'], messageType);
    }, 300000);
  }

  //etsy_product_collection Consumer
  @MessagePattern('etsy_product_collection')
  async etsy(@Ctx() context: KafkaContext) {
    const originalMessage = context.getMessage();
    const value = await originalMessage.value['body'];
    this.accessToken = await this.tokenService.getByClient(value['sellerId']);
  }

  //listing product
  @Get('listing')
  async list(@Query() data: listingDto) {
    this.accessToken = await this.tokenService.getByClient(data.sellerId);
    const headers = {
      'Content-Language': 'en-US',
      Authorization: `Bearer ${this.accessToken['access_token']}`,
    };
    const method = 'GET';
    const url = `${Constant.EBAYURL}/sell/inventory/v1/inventory_item/${data.sku}/product_compatibility`;
    const tokValue$ = await this.ebayGlobalService.ebayHttpRequest(
      null,
      headers,
      method,
      url,
    );
    const tokenData = await lastValueFrom(tokValue$);
    const msg = 'Product Successfully listed !';
    this.loggerService.log(msg, tokenData.status);
    return tokenData.data;
  }
}
