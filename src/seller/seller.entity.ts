import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('seller')
export class SellerEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  sellerId: string;

  @Column('jsonb', { nullable: true })
  sellerPlatformData?: any;
}
