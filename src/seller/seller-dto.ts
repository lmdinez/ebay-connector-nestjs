import { ApiProperty } from '@nestjs/swagger';

export class SellerDto {
  @ApiProperty({
    type: String,
    description: 'SellerId',
  })
  sellerId: number;

  @ApiProperty({
    type: String,
    description: 'Platform Id',
  })
  platformId: number;

  @ApiProperty({
    type: String,
    description: 'Collection Id',
  })
  collectionId: number;
}

export class listingDto {
  @ApiProperty({
    type: String,
    description: 'SellerId',
  })
  sellerId: number;
  @ApiProperty({
    type: String,
    description: 'sku',
  })
  sku: string;
}
