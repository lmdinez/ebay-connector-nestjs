/*
https://docs.nestjs.com/providers#services
*/

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SellerEntity } from './seller.entity';
import { Repository } from 'typeorm';
import { RedisCacheService } from 'src/redis/redis-cache.service';
import { ProductService } from 'src/product/product.service';

@Injectable()
export class SellerService {
  constructor(
    @InjectRepository(SellerEntity)
    private sellerRepository: Repository<SellerEntity>,
    private cacheManager: RedisCacheService,
    private productService: ProductService,
  ) {}

  async findAll() {
    return await this.sellerRepository.find();
  }

  async setSomeValue(KEY, value) {
    return await this.cacheManager.set(KEY, value);
  }

  async getSomeValue(KEY) {
    return await this.cacheManager.get(KEY);
  }

  async delSomeValue(KEY) {
    return await this.cacheManager.del(KEY);
  }

  // async getData(data: any) {
  //   const value = data
  //   const productColletionData = await this.productService.getByProductCollection(value.collection);
  //   await this.cacheManager.set(value.collection, productColletionData['productData'])
  // }
}
